
#include "bloom-widget-link-factory.h"
#include "bloom-widget-link.h"
#include "bloom-widget-dbus-adaptor.h"

static int instance_count = 1;

QDBusObjectPath BloomWidgetLinkFactory::create()
{
    QString object_path;
    QDBusConnection connection = QDBusConnection::sessionBus();

    BloomWidgetLink *widget = new BloomWidgetLink();
    new WidgetAdaptor(widget);

    object_path.sprintf("/Link/%d", instance_count++);
    bool ret = connection.registerService("org.agorabox.Bloom.Widget.Link");
    ret = connection.registerObject(object_path, widget);

    return QDBusObjectPath(object_path);
}

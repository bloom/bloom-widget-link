
#ifndef BLOOM_WIDGET_LINK_FACTORY_H
#define BLOOM_WIDGET_LINK_FACTORY_H

#include <gtk/gtk.h>
#include <QtCore>
#include <QtDBus/QtDBus>

#include "bloom-widget-factory-dbus-adaptor.h"

class BloomWidgetLinkFactory : public QObject
{
    Q_OBJECT

public Q_SLOTS:
    QDBusObjectPath create();
};

#endif

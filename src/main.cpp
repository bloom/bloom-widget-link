#include "bloom-widget-link.h"
#include "bloom-widget-dbus-adaptor.h"
#include "bloom-widget-qt.h"

#define BLOOM_WIDGET_LINK_SERVICE "org.agorabox.Bloom.Widget.Link"

int main(int argc, char **argv) {
    bool ret;
    QApplication app(argc, argv);
    QDBusConnection connection = QDBusConnection::sessionBus();

    BloomWidgetLink *widget = new BloomWidgetLink();
    new WidgetAdaptor(widget);

    QString service = get_free_service_name(BLOOM_WIDGET_LINK_SERVICE);
    ret = connection.registerService(service);

    ret = connection.registerObject("/Widget", widget);

    return app.exec();
}


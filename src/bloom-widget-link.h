#ifndef _BLOOM_WIDGET_LINK_H_
#define _BLOOM_WIDGET_LINK_H_

#include <gtk/gtk.h>

#include <QtGui>
#include <QUrl>

#include <gtk/gtkcontainer.h>
#include <gdk/gdk.h>
#include <gdk/gdkx.h>

#define MOZILLA_INTERNAL_API
#define MOZILLA_CLIENT

#include "mozilla-config.h"
#include "gtkmozembed.h"
#include "gtkmozembed_internal.h"
#include <nsIWebBrowser.h>
#include <nsCOMPtr.h>
#include <nsIDOMWindow.h>
#include "gconfitem.h"

class BloomWidgetLink : public QObject
{
    Q_OBJECT

public:
    BloomWidgetLink();
    virtual ~BloomWidgetLink();

    void go_back();
    void go_forward();
    void go_home();
    void reload();
    void stop();
    void zoom_in();
    void zoom_out();
    void set_zoom(float);
    void snapshot();
    void set_offline();

public Q_SLOTS:
    int create(int container, const QString& gconf_entry);

public slots:
    void changed_url();
    void toggle_toolbar();
    void changed_zoom();
    void network_state_changed(unsigned int);

public:
    GConfItem *homepage;
    GConfItem *schema;
    GConfItem *show_toolbar;
    GConfItem *zoom;
    GtkMozEmbed *webview;
    GtkPlug *plug;
    GtkVBox *vbox;
    GtkHBox *toolbar;
    GtkWidget *hline;
    GtkWidget *home_button;
    GtkWidget *back_button;
    GtkWidget *forward_button;
    GtkWidget *reload_button;
    GtkWidget *zoom_in_button;
    GtkWidget *zoom_out_button;

    quint32 net_state;
};

#endif /* _BLOOM_WIDGET_LINK_H_ */

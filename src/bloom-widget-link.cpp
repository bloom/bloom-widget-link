/*
 * bloom-widget-link
 *
 * Copyright (C) 2010, Agorabox.
 *
 * Author: BAUBEAU Sylvain
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "bloom-widget-link.h"

#include <algorithm>
#include <locale.h>
#include <libintl.h>

#include <QDBusConnection>
#include <QDBusInterface>
#include <QDBusReply>

#define _(STRING)    gettext(STRING)

#define OFFLINE_PAGE "file://" PKGDATADIR "/offline.html"
#define NETWORK_MANAGER_SERVICE "org.freedesktop.NetworkManager"
#define NETWORK_MANAGER_OBJECTPATH "/org/freedesktop/NetworkManager"
#define NETWORK_MANAGER_INTERFACE "org.freedesktop.NetworkManager"

using namespace std;

void home(GtkWidget *button, gpointer data);
void back(GtkWidget *button, gpointer data);
void forward(GtkWidget *button, gpointer data);
void reload(GtkWidget *button, gpointer data);
void zoom_in(GtkWidget *button, gpointer data);
void zoom_out(GtkWidget *button, gpointer data);
void new_window(GtkMozEmbed *embed, GtkMozEmbed **retval, guint chromemask, gpointer data);

QString get_widget_schema(QString name, QString schema_name = "")
{
    QString schema;
    schema = "/schemas/desktop/bloom/widget/" + schema_name + "/" + name;
    GConfItem gconf_item(schema);
    qDebug() << "Looking for schema" << schema << " " << schema_name << " " << gconf_item.value().isNull();
    if (!schema_name.isEmpty() && !gconf_item.value().isNull())
        return schema;
    else
        return "/schemas/desktop/bloom/widget/" + name;
}

BloomWidgetLink::BloomWidgetLink()
{
    net_state = -1;
    homepage = NULL;
    QDBusConnection system_bus = QDBusConnection::systemBus();
    system_bus.connect(NETWORK_MANAGER_SERVICE, NETWORK_MANAGER_OBJECTPATH,
                       NETWORK_MANAGER_INTERFACE, "StateChanged",
                       this, SLOT(network_state_changed(unsigned int)));
    QDBusInterface nm(NETWORK_MANAGER_SERVICE, NETWORK_MANAGER_OBJECTPATH,
                      NETWORK_MANAGER_INTERFACE, system_bus);
    if (!nm.isValid()) {
        qWarning() << "Could not find NetworkManager";
        return;
    }
    QDBusReply<quint32> reply = nm.call("state");
    if (reply.isValid())
        net_state = reply.value();
}

BloomWidgetLink::~BloomWidgetLink()
{
    if (homepage)
        delete homepage;
}

int BloomWidgetLink::create(int container, const QString& gconf_entry)
{
    webview = NULL;
    home_button = back_button = forward_button = reload_button = NULL;
    schema = new GConfItem(gconf_entry + "/schema");
    homepage = new GConfItem(gconf_entry + "/homepage", get_widget_schema("homepage", *schema));
    show_toolbar = new GConfItem(gconf_entry + "/toolbar", get_widget_schema("toolbar", *schema));
    zoom = new GConfItem(gconf_entry + "/zoom", get_widget_schema("zoom", *schema));

    plug = (GtkPlug*) gtk_plug_new(0);
    vbox = (GtkVBox*) gtk_vbox_new(false, 0);
    toolbar = (GtkHBox*) gtk_hbox_new(false, 0);
    hline = gtk_hseparator_new();

    GdkColor white;
    gdk_color_parse("#fff" ,&white);
    gtk_widget_modify_bg(GTK_WIDGET(plug), GTK_STATE_NORMAL, &white);

    gtk_moz_embed_set_path(XUL_PATH);

    webview = (GtkMozEmbed*) gtk_moz_embed_new();

    if(!webview) {
        qCritical() << "Failed to create webview";
        return 0;
    }

    GtkWidget *scroll = gtk_scrolled_window_new(NULL, NULL);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scroll),
                                   GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

    gtk_container_add(GTK_CONTAINER(plug), GTK_WIDGET(vbox));
    gtk_container_add(GTK_CONTAINER(vbox), scroll);
    gtk_container_add(GTK_CONTAINER(scroll), GTK_WIDGET(webview));

    gtk_box_set_child_packing(GTK_BOX(vbox), scroll, TRUE, TRUE, 0, GTK_PACK_END);

    gtk_widget_show(GTK_WIDGET(vbox));
    gtk_widget_show(GTK_WIDGET(webview));
    gtk_widget_show(scroll);
    gtk_widget_show(GTK_WIDGET(plug));

    gtk_widget_activate(GTK_WIDGET(webview));
    gtk_widget_grab_focus(GTK_WIDGET(webview));

    QObject::connect(homepage, SIGNAL(valueChanged()), this, SLOT(changed_url()));
    QObject::connect(show_toolbar, SIGNAL(valueChanged()), this, SLOT(toggle_toolbar()));
    QObject::connect(zoom, SIGNAL(valueChanged()), this, SLOT(changed_zoom()));

    emit changed_url();
    emit changed_zoom();
    emit toggle_toolbar();

    return gtk_plug_get_id(plug); // 0;
}

void BloomWidgetLink::toggle_toolbar()
{
    if (*show_toolbar && !home_button) {
        GtkWidget *home_button = gtk_button_new();
        GtkWidget *back_button = gtk_button_new();
        GtkWidget *forward_button = gtk_button_new();
        GtkWidget *reload_button = gtk_button_new();
        GtkWidget *zoom_in_button = gtk_button_new();
        GtkWidget *zoom_out_button = gtk_button_new();

        gtk_button_set_image(GTK_BUTTON(home_button), gtk_image_new_from_icon_name("user-home", GTK_ICON_SIZE_MENU));
        gtk_button_set_image(GTK_BUTTON(back_button), gtk_image_new_from_icon_name("go-previous", GTK_ICON_SIZE_MENU));
        gtk_button_set_image(GTK_BUTTON(forward_button), gtk_image_new_from_icon_name("go-next", GTK_ICON_SIZE_MENU));
        gtk_button_set_image(GTK_BUTTON(reload_button), gtk_image_new_from_icon_name("reload", GTK_ICON_SIZE_MENU));
        gtk_button_set_image(GTK_BUTTON(zoom_in_button), gtk_image_new_from_icon_name("zoom-in", GTK_ICON_SIZE_MENU));
        gtk_button_set_image(GTK_BUTTON(zoom_out_button), gtk_image_new_from_icon_name("zoom-out", GTK_ICON_SIZE_MENU));

        gtk_button_set_relief(GTK_BUTTON(home_button), GTK_RELIEF_NONE);
        gtk_button_set_relief(GTK_BUTTON(back_button), GTK_RELIEF_NONE);
        gtk_button_set_relief(GTK_BUTTON(forward_button), GTK_RELIEF_NONE);
        gtk_button_set_relief(GTK_BUTTON(reload_button), GTK_RELIEF_NONE);
        gtk_button_set_relief(GTK_BUTTON(zoom_in_button), GTK_RELIEF_NONE);
        gtk_button_set_relief(GTK_BUTTON(zoom_out_button), GTK_RELIEF_NONE);

        gtk_container_add(GTK_CONTAINER(vbox), GTK_WIDGET(toolbar));
        gtk_container_add(GTK_CONTAINER(vbox), hline);

        gtk_container_add(GTK_CONTAINER(toolbar), GTK_WIDGET(home_button));
        gtk_container_add(GTK_CONTAINER(toolbar), GTK_WIDGET(back_button));
        gtk_container_add(GTK_CONTAINER(toolbar), GTK_WIDGET(forward_button));
        gtk_container_add(GTK_CONTAINER(toolbar), GTK_WIDGET(reload_button));
        gtk_container_add(GTK_CONTAINER(toolbar), GTK_WIDGET(zoom_in_button));
        gtk_container_add(GTK_CONTAINER(toolbar), GTK_WIDGET(zoom_out_button));

        gtk_box_set_child_packing(GTK_BOX(toolbar), GTK_WIDGET(home_button), FALSE, FALSE, 0, GTK_PACK_START);
        gtk_box_set_child_packing(GTK_BOX(toolbar), GTK_WIDGET(back_button), FALSE, FALSE, 0, GTK_PACK_START);
        gtk_box_set_child_packing(GTK_BOX(toolbar), GTK_WIDGET(forward_button), FALSE, FALSE, 0, GTK_PACK_START);
        gtk_box_set_child_packing(GTK_BOX(toolbar), GTK_WIDGET(reload_button), FALSE, FALSE, 0, GTK_PACK_START);
        gtk_box_set_child_packing(GTK_BOX(toolbar), GTK_WIDGET(zoom_in_button), FALSE, FALSE, 0, GTK_PACK_START);
        gtk_box_set_child_packing(GTK_BOX(toolbar), GTK_WIDGET(zoom_out_button), FALSE, FALSE, 0, GTK_PACK_START);
        gtk_box_set_child_packing(GTK_BOX(vbox), GTK_WIDGET(toolbar), FALSE, FALSE, 0, GTK_PACK_START);
        gtk_box_set_child_packing(GTK_BOX(vbox), hline, FALSE, FALSE, 0, GTK_PACK_START);

        gtk_widget_show(GTK_WIDGET(toolbar));

        gtk_widget_show(home_button);
        gtk_widget_show(back_button);
        gtk_widget_show(forward_button);
        gtk_widget_show(reload_button);
        gtk_widget_show(zoom_in_button);
        gtk_widget_show(zoom_out_button);
        gtk_widget_show(hline);

        g_signal_connect(G_OBJECT(home_button), "clicked", G_CALLBACK(home), this);
        g_signal_connect(G_OBJECT(back_button), "clicked", G_CALLBACK(back), this);
        g_signal_connect(G_OBJECT(forward_button), "clicked", G_CALLBACK(forward), this);
        g_signal_connect(G_OBJECT(reload_button), "clicked", G_CALLBACK(::reload), this);
        g_signal_connect(G_OBJECT(zoom_in_button), "clicked", G_CALLBACK(::zoom_in), this);
        g_signal_connect(G_OBJECT(zoom_out_button), "clicked", G_CALLBACK(::zoom_out), this);

        g_signal_connect(G_OBJECT(webview), "new_window", G_CALLBACK(new_window), this);
    }
    else {
        qDebug() << "Removing toolbar is not implemented yet";
    }
}

void BloomWidgetLink::set_offline()
{
    set_zoom(1.0);
    gtk_moz_embed_load_url(webview, OFFLINE_PAGE);
}

void BloomWidgetLink::changed_url()
{
    qDebug() << "Loading" << QString(*homepage);
    if (webview) {
        if (net_state == 3 || QString(*homepage).startsWith("file://")) {
            emit changed_zoom();
            gtk_moz_embed_load_url(webview, QString(*homepage).toLatin1());
        }
        else {
            set_offline();
        }
    }
}

void BloomWidgetLink::changed_zoom()
{
    if (webview) {
        nsCOMPtr<nsIWebBrowser>         mWebBrowser;
        nsCOMPtr<nsIDOMWindow>          mDOMWindow;

        gtk_moz_embed_get_nsIWebBrowser(GTK_MOZ_EMBED(webview), getter_AddRefs(mWebBrowser));
        mWebBrowser->GetContentDOMWindow(getter_AddRefs(mDOMWindow));
        if(NULL == mDOMWindow) {
            g_warning("could not retrieve DOM window...");
        }
        mDOMWindow->SetTextZoom (float(*zoom));
    }
}

void BloomWidgetLink::stop()
{
    if (webview) {
        gtk_moz_embed_stop_load(webview);
    }
}

void BloomWidgetLink::snapshot()
{
    GtkAllocation allocation;
    gtk_widget_get_allocation(GTK_WIDGET(webview), &allocation);
    int width = allocation.width;
    int height = allocation.height;
    GdkPixbuf *pixbuf = gdk_pixbuf_new(GDK_COLORSPACE_RGB, false, 8, width, height);
    gdk_pixbuf_get_from_drawable(pixbuf, GDK_DRAWABLE(webview), gdk_colormap_get_system(),
                                 0, 0, 0, 0, width, height);
    gdk_pixbuf_save(pixbuf, "/tmp/snapshot.png", "png", NULL, NULL);
}

void BloomWidgetLink::network_state_changed(unsigned int new_state)
{
    if (net_state == 3 && new_state != 3) {
        stop();
    }
    net_state = new_state;
    emit changed_url();
}

void BloomWidgetLink::go_back()
{
    if (webview) {
        gtk_moz_embed_go_back(webview);
    }
}

void BloomWidgetLink::go_forward()
{
    if (webview) {
        gtk_moz_embed_go_forward(webview);
    }
}

void BloomWidgetLink::go_home()
{
    changed_url();
}

void BloomWidgetLink::reload()
{
    if (webview) {
        gtk_moz_embed_reload(webview, GTK_MOZ_EMBED_FLAG_RELOADNORMAL);
    }
}

void BloomWidgetLink::set_zoom(float ratio)
{
    *zoom = ratio;
    emit changed_zoom();
}

void BloomWidgetLink::zoom_in()
{
    *zoom = double(*zoom) + 0.1;
    emit changed_zoom();
}

void BloomWidgetLink::zoom_out()
{
    *zoom = double(max(0.1, double(*zoom) - 0.1));
    emit changed_zoom();
}

void back(GtkWidget *button, gpointer data)
{
    ((BloomWidgetLink*) data)->go_back();
}

void forward(GtkWidget *button, gpointer data)
{
    ((BloomWidgetLink*) data)->go_forward();
}

void home(GtkWidget *button, gpointer data)
{
    ((BloomWidgetLink*) data)->go_home();
}

void reload(GtkWidget *button, gpointer data)
{
    ((BloomWidgetLink*) data)->reload();
}

void zoom_in(GtkWidget *button, gpointer data)
{
    ((BloomWidgetLink*) data)->zoom_in();
}

void zoom_out(GtkWidget *button, gpointer data)
{
    ((BloomWidgetLink*) data)->zoom_out();
}

void new_window(GtkMozEmbed *embed, GtkMozEmbed **retval, guint chromemask, gpointer data)
{
    *retval = NULL;
}

